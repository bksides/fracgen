#ifndef FRACGEN_CAMERA_HPP
#define FRACGEN_CAMERA_HPP

#include <glm/glm.hpp>

#include <interpolator.hpp>

class Camera : public Interpolator {
public:
    const glm::mat2x2 render_area;
    Camera(glm::mat2x2 render_area);
    Interpolator* interpolate(const Interpolator* camera, float bias) const;
};

#endif