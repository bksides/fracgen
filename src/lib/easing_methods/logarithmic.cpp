#include <cmath>

#include <easing_methods.hpp>

float EasingMethod::logarithmic_fn(float x) {
    const float stop = .01;
    const float log_stop = log(stop);
    const float current = x*log_stop;
    return (1-exp(current))/(1-stop);
}

float EasingMethod::sine_fn(float x) {
    return (sin(x*2*M_PI)+1)/2;
}

float EasingMethod::half_sine_fn(float x) {
    return sin(x*M_PI);
}

float EasingMethod::cosine_fn(float x) {
    return (1-cos(x*2*M_PI))/2;
}

float EasingMethod::half_cosine_fn(float x) {
    return (1-cos(x*M_PI))/2;
}