# Fractal Generator

This is a command-line based fractal generator.  It can generate still images or sequences of images, which can be compiled into an animation.

## Compiling

You will need a machine capable of compiling and running a CUDA program; this means installing the
[nVidia CUDA toolkit](https://developer.nvidia.com/cuda-toolkit) for compilation and ensuring that you have an nvidia GPU.

You will also need the [GLM](https://glm.g-truc.net/0.9.9/), [CImg](https://cimg.eu/), and [nlohmann JSON parsing library](https://github.com/nlohmann/json) header files somewhere
your compiler will know to look for them, in addition to [libpng](http://www.libpng.org/pub/png/libpng.html) which
is very likely already present on your system.

[Cmake](https://cmake.org/download/) can be used to simplify the compilation process.  On linux using CMake, the following set of commands builds an executable called `fracgen` in a subdirectory called build:

```
$ mkdir build
$ cd build
$ cmake ..
$ make fracgen
```

This may vary somewhat depending on your system, particularly on a Windows machine.

If CMake doesn't work for you, you may have to invoke `nvcc` yourself to compile the program.  The following command has worked on my system:

```
nvcc <list of files in src/lib> main.cpp -o build/fracgen --std=c++17 -lpng
```

But, again, YMMV, especially since this project is changing rapidly at the moment.

## Usage

```
./fracgen [-c config_file] [-x resolution_x] [-y resolution_y] [-f num_frames] [-s num_steps] [-o output_file]
```

- `-c`: the path to a [config file](#configuration-files) (default: none)
- `-x`: the horizontal resolution (default: 400)
- `-y`: the vertical resolution (default: 400)
- `-f`: the number of frames to generate; if 1, a still image will be generated and the zoom arguments will be ignored (default: 1)
- `-s`: the number of iterations to carry out; more iterations will create a more detailed fractal (default: 100)
- `-o`: the name of the file to save the render to; if `-f 0` is used, the file will be `<output_file>.png`; otherwise, multiple files will be produced, each named `<output_file>-i.png`, where `i` is the number of the frame from 0 to `num_frames-1`.  `i` will be padded by zeroes up to four digits (default: `"render"`)

## Configuration Files

While it is (or should be) possible to specify all configuration
parameters through the command line, it can be more convenient to specify them
in a file.  To enable this, the `-c` option can be used on the command line to
specify a JSON configuration file satisfying the following structure (all top-level fields are optional; however, if a top-level element is included, all nested sub-elements are required):
```
{
    "resolution": <ivec2>,
    "zoom_start": {
        "top_left": {
            "x": <float>,
            "y": <float>
        },
        "bottom_right": {
            "x": <float>,
            "y": <float>
        }
    },
    "zoom_end": {
        "top_left": {
            "x": <float>,
            "y": <float>
        },
        "bottom_right": {
            "x": <float>,
            "y": <float>
        }
    },
    "frames": <int>,
    "steps": <int>,
    "output": <string>
    "easing": <string> | <string[]>
}
```

## Examples

Generate a 5000x5000 image called `render.png` of the fractal after 10000 iterations.

```
./fracgen -x 5000 -y 5000 -s 10000 -o render
```

![Rendered fractal](https://gitlab.com/bksides/fracgen/-/raw/media/examples/render.png)

Generate a sequence of 200 renders of 5000 iterations, each 1000x1000 pixels, zooming to a particular area.  This would create 200 images,
named from `render-0000.png` to `render-0199.png`.
```
./fracgen -x 1000 -y 1000 -f 200 -s 5000 --zoom-x-start -0.775 --zoom-x-end -0.75 --zoom-y-start 0.625 --zoom-y-end 0.65 -o render
```
These could be compiled as frames of a video or GIF:

![Fractal zoom video](https://gitlab.com/bksides/fracgen/-/raw/media/examples/render.gif)

## Roadmap

The following is an outline of planned features.

- ~~implement JSON config file parsing~~
- ~~implement zooming~~
    - ~~JSON~~
    - add command line args
- ~~implement easing~~
    - ~~JSON~~
    - add command line args
- Configurable easing methods
    - ~~JSON~~
    - command line
    - ~~support composition~~
    - support parametrization
- implement animation API
    - ~~General Animation infrastructure~~
    - Split configuration into start-state and end-state
    - Implement Interpolator for Evolver class to allow arbitrary animation
- Implement evolvers
    - Make evolver an abstract class to allow arbitrary evolvers
    - three-body
        - ~~basic generation~~
        - configurable parameters
            - number of bodies
            - positions of bodies
            - masses of bodies
            - number of dimensions
        - find a way to decide when to stop taking more steps without relying on collision with an arbitrary sphere around attraction point;
          the goal is to figure out what point it settles on, not what sphere it collides with first
    - koch snowflake
    - mandelbrot
- separate image generation from fractal evolution
    - generate first and store data; *then* create images
    - remove any dependency on image libraries from evolver
    - allow selecting which fields of evolver are mapped to a given pixel/color
- optimize generation
    - sample fractal on an independent grid to permit reuse of results  in later frames
    - rather than go frame by frame, figure out ahead of time
    which grid points need what calculations and do them all at once
- progress polling (as opposed to generating one frame at a time)