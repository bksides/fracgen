#ifndef FRACGEN_CONFIG_HPP
#define FRACGEN_CONFIG_HPP

#include <getopt.h>
#include <glm/glm.hpp>
#include <cstdio>

#include <nlohmann/json.hpp>
#include <easing_methods.hpp>

template<typename T>
class ConfigField {
private:
    T def;
    T val;
    bool is_set = false;
public:
    ConfigField(const T& val) : val(val), def(val) {};
    const T& get() const;
    void set(const T& val);
    bool isSet() const;
    void unset();
};

class Config {
public:
    ConfigField<glm::ivec2> resolution;
    ConfigField<glm::mat2x2> zoom_start;
    ConfigField<glm::mat2x2> zoom_end;
    ConfigField<const EasingMethod*> easing_method;
    ConfigField<int> frames;
    ConfigField<int> steps;
    ConfigField<std::string> input;
    ConfigField<std::string> output;

    Config(glm::ivec2 resolution = {400, 400},
        glm::mat2x2 zoom_start = {
            {-10, -10},
            { 10,  10}
        },
        glm::mat2x2 zoom_end = {
            {-10, -10},
            { 10,  10}
        },
        const EasingMethod* easing_method = &EasingMethod::logarithmic,
        int frames = 1,
        int steps = 100,
        const char* input = "",
        const char* output = "render"
    ) : resolution(resolution), zoom_start(zoom_start), zoom_end(zoom_end), easing_method(easing_method), frames(frames), steps(steps), input(input), output(output) {}

    static Config from_args(int argc, char** argv);
    void merge(const Config& c);
};

void to_json(nlohmann::json& j, const Config& c);

void from_json(const nlohmann::json& j, Config& c);

#endif