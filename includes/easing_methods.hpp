#ifndef FRACGEN_EASING_METHODS_HPP
#define FRACGEN_EASING_METHODS_HPP

#include <functional>
#include <string>
#include <unordered_map>
#include <memory>
#include <nlohmann/json.hpp>
#include <iostream>

class EasingMethod {
private:
    static inline std::unordered_map<std::string, std::unique_ptr<const EasingMethod>> registrar;
    static float logarithmic_fn(float x);
    static float half_sine_fn(float x);
    static float sine_fn(float x);
    static float half_cosine_fn(float x);
    static float cosine_fn(float x);

    std::function<float (float)> f;
    const std::string* name;
    EasingMethod() = default;
    EasingMethod(std::function<float (float)> f, const std::string& n) : f(f) {}
public:
    static const EasingMethod& logarithmic;
    static const EasingMethod& half_sine;
    static const EasingMethod& sine;
    static const EasingMethod& half_cosine;
    static const EasingMethod& cosine;

    EasingMethod(std::function<float (float)> f) : f(f) {};
    static const EasingMethod* create(std::function<float (float)> f, const std::string& n);
    static const EasingMethod* add_to_registrar(EasingMethod&& em, const std::string& n);

    static EasingMethod from_json(const nlohmann::json& j) {
        std::cout << j.type_name() << "\n";
        switch(j.type()) {
            case nlohmann::json::value_t::array: {
                EasingMethod new_em([](float x) -> float {
                    return x;
                });
                for(auto iter = j.rbegin(); iter != j.rend(); ++iter) {
                    EasingMethod composing_em = from_json(*iter);
                    new_em = composing_em(new_em);
                }
                return new_em;
            }
            case nlohmann::json::value_t::string: {
                return *EasingMethod::getByName(j.get<std::string>());
            }
            default: {
                throw nlohmann::detail::type_error::create(302, "EasingMethod objects can only be deserialized from strings or arrays of strings", j);
            }
        }
    }

    const std::string* getName() const;
    EasingMethod operator()(const EasingMethod& m) const;
    float operator()(float m) const;
    static const EasingMethod* getByName(const std::string&);
};

#endif