#include <easing_methods.hpp>
#include <unordered_map>

const std::string logarithmic_name = "logarithmic";
const std::string half_sine_name = "half_sine";
const std::string sine_name = "sine";
const std::string half_cosine_name = "half_cosine";
const std::string cosine_name = "cosine";
const EasingMethod& EasingMethod::logarithmic = *EasingMethod::create(logarithmic_fn, logarithmic_name);
const EasingMethod& EasingMethod::half_sine = *EasingMethod::create(half_sine_fn, half_sine_name);
const EasingMethod& EasingMethod::sine = *EasingMethod::create(sine_fn, sine_name);
const EasingMethod& EasingMethod::half_cosine = *EasingMethod::create(half_cosine_fn, half_cosine_name);
const EasingMethod& EasingMethod::cosine = *EasingMethod::create(cosine_fn, cosine_name);

const EasingMethod* EasingMethod::create(std::function<float (float)> f, const std::string& name) {
    if(registrar.count(name)) {
        return nullptr;
    }

    const EasingMethod* em_p = new EasingMethod(f, name);
    registrar[name] = std::unique_ptr<const EasingMethod>(em_p);
    return registrar[name].get();
}

const EasingMethod* EasingMethod::add_to_registrar(EasingMethod&& em, const std::string& n) {
    if(registrar.count(*em.name)) {
        return nullptr;
    }

    registrar[*em.name] = std::make_unique<EasingMethod>(em);
    return registrar[*em.name].get();
}

const std::string* EasingMethod::getName() const {
    return this->name;
}

EasingMethod EasingMethod::operator()(const EasingMethod& m) const {
    const std::function<float(float)>& fn1 = this->f;
    const std::function<float(float)>& fn2 = m.f;
    return EasingMethod([fn1, fn2](float x){
        return fn1(fn2(x));
    });
}

float EasingMethod::operator()(float x) const {
    return f(x);
}

const EasingMethod* EasingMethod::getByName(const std::string& name) {
    auto res = registrar.find(name);
    if(res != registrar.end()) {
        return res->second.get();
    }
    return nullptr;
}