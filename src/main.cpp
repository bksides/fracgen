#define cimg_use_png
#define cimg_display 0

#include <iostream>
#include <glm/glm.hpp>
#include <CImg.h>

#include <config.hpp>
#include <evolver.hpp>
#include <camera.hpp>
#include <animation.hpp>
#include <easing_methods.hpp>

using namespace cimg_library;

template <int cols, int rows, typename T, glm::qualifier Q>
glm::mat<cols, rows, T, Q> hadamard_power(glm::mat<cols, rows, T, Q> m1, glm::mat<cols, rows, T, Q> m2) {
    glm::mat<cols, rows, T, Q> ret;
    for(int r = 0; r < rows; ++r) {
        for(int c = 0; c < cols; ++c) {
            ret[c][r] = (T)(pow((float)m1[c][r], (float)m2[c][r]));
        }
    }
    return ret;
}

template <int cols, int rows, typename T, glm::qualifier Q>
glm::mat<cols, rows, T, Q> component_wise_abs(glm::mat<cols, rows, T, Q> m) {
    for(int c = 0; c < cols; ++c) {
        m[c] = glm::abs(m[c]);
    }
    return m;
}

int main(int argc, char** argv){
    Config cfg = Config::from_args(argc, argv);

    char filename[100];
    

    if(cfg.frames.get() > 1) {
        Camera start(cfg.zoom_start.get());
        Camera end(cfg.zoom_end.get());
        Animation camera_animation(start, end, *cfg.easing_method.get());

        for(int i = 0; i < cfg.frames.get(); ++i) {
            float progress = (float)i/(cfg.frames.get()-1);
            const glm::mat2x2& current_camera = ((Camera&)camera_animation.state_at(progress)).render_area;

            //std::cout << "current_camera: {{" << current_camera[0].x << ", " << current_camera[0].y << "}, {" << current_camera[1].x << ", " << current_camera[1].y << "}}\n\n";
            // At last, we've fininshed doing the zoom logic.  Now we do the evolution...
            Evolver evolver(cfg, current_camera);
            evolver.evolve(0.01, cfg.steps.get());
            snprintf(filename, 100, "%s-%04d.png", cfg.output.get().c_str(), i);
            evolver.render().save_png(filename);
        }
    } else {
        Evolver evolver(cfg, cfg.zoom_start.get());
        evolver.evolve(.01, cfg.steps.get());
        snprintf(filename, 100, "%s.png", cfg.output.get().c_str());
        evolver.render().save_png(filename);
    }
}