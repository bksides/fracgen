#include <camera.hpp>

Camera::Camera(glm::mat2x2 area) : render_area(area) {}

Interpolator* Camera::interpolate(const Interpolator* camera, float bias) const {
    return new Camera((1-bias)*this->render_area + bias*((Camera*)camera)->render_area);
}