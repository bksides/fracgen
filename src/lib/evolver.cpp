#include <evolver.hpp>
#include <cuda.h>
#include <cuda_runtime_api.h>

#include <config.hpp>

Evolver::Evolver(const Config& cfg, const glm::mat2x2 camera) :
    resolution(cfg.resolution.get()),
    camera(camera)
{
    const glm::ivec2* res;

    this->particle_cache = (particle*)malloc(this->resolution.x * this->resolution.y * sizeof(particle));

    for(int y = 0; y < this->resolution.y; y++) {
        for(int x = 0; x < this->resolution.x; x++) {
            this->particle_cache[y*resolution.x + x] = particle{
                .position = camera[0] + (glm::vec2(x,y)/(glm::vec2)this->resolution)*(camera[1]-camera[0]),
                .velocity = {0, 0}
            };
        }
    }
    int particle_arr_size = this->resolution.x * this->resolution.y * sizeof(particle);
    cudaMalloc((void**)&device_particles, particle_arr_size);
    cudaMemcpy(device_particles, particle_cache, particle_arr_size, cudaMemcpyHostToDevice);
}

Evolver::~Evolver() {
    delete this->particle_cache;
    cudaFree(this->device_particles);
}

void Evolver::evolve(float seconds, int evolutions) {
    this->invalidated = true;
    dispatch_kernel_evolve(this->device_particles, this->resolution, seconds, evolutions);
}

cimg_library::CImg<unsigned char> Evolver::render() {
    cimg_library::CImg<unsigned char> ret(resolution.x, resolution.y, 1, 3, 0);
    for(int y = 0; y < resolution.y-1; ++y) {
        for(int x = 0; x < resolution.x-1; ++x) {
            particle p[4] = {
                (*this)[glm::ivec2(x, y)],
                (*this)[glm::ivec2(x+1, y)],
                (*this)[glm::ivec2(x, y+1)],
                (*this)[glm::ivec2(x+1, y+1)]
            };
            for(int g = 0; g < 3; ++g) {
                float distance = 0;
                for(int i = 0; i < 4; ++i) {
                    glm::vec2 displacement = gravitators[g] - p[i].position;
                    distance += glm::length(displacement);
                }
                distance /= 4;
                ret(x,y,0,g) = (int)(255/(distance+1));
            }
        }
    }
    return ret;
}

const particle& Evolver::operator[](glm::ivec2 ind) {
    if(invalidated) {
        int particle_arr_size = resolution.x * resolution.y * sizeof(particle);
        cudaMemcpy(particle_cache, device_particles, particle_arr_size, cudaMemcpyDeviceToHost);
        invalidated = false;
    }
    return particle_cache[ind.y*resolution.x + ind.x];
}