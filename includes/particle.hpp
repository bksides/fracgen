#ifndef FRACGEN_PARTICLE_HPP
#define FRACGEN_PARTICLE_HPP

#include <glm/glm.hpp>

struct particle{
    glm::vec2 position;
    glm::vec2 velocity;
};

#endif