#ifndef FRACGEN_INTERPOLATOR_HPP
#define FRACGEN_INTERPOLATOR_HPP

class Interpolator {
public:
    virtual Interpolator* interpolate(const Interpolator* other, float bias) const = 0; 
};

#endif