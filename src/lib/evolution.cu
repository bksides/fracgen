#include <glm/glm.hpp>
#include <particle.hpp>

__global__ void kernel_evolve(particle* particles, glm::ivec2 resolution, float ds, int evolutions) {
    // These are our gravitators
    ///TODO: really we ought to be passing these in
    static glm::mat3x2 gravitators = {
        {-.5, -.5},
        { .5, -.5},
        {  0,  .5}
    };

    // Determine how to distribute workload between blocks/threads
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    particle& particle = particles[i];

    // We bound the timestep so that a particle never moves too much in a single step; this
    // avoids having particles fly off when they have large forces applied
    float speed = particle.velocity.length();
    float dt = speed ? ds / speed : ds;
    dt = dt < ds*10 ? dt : ds*10;

    // do the evolution steps
    for(int step = 0; step < evolutions; ++step) {
        glm::vec2 aggregate_force = {0.0f, 0.0f};
        for(int g = 0; g < 3; ++g) {
            glm::vec2 displacement = gravitators[g] - particle.position;
            float distance = glm::length(displacement) + 0.07;
            glm::vec2 direction = displacement / distance;
            if(distance > 0.14) {
                aggregate_force += direction/(distance*distance);
            } else {
                particle.velocity = {0, 0};
                aggregate_force = {0, 0};
                break;
            }
        }
        particle.velocity += aggregate_force * dt;
        particle.position += particle.velocity * dt;
    }
}

// CPU-level wrapper
void dispatch_kernel_evolve(particle* particles, glm::ivec2 resolution, float dt, int evolutions) {
    int num_particles = resolution.x * resolution.y;
    kernel_evolve<<<num_particles/1024 + ((num_particles % 1024) ? 1 : 0), 1024>>>(particles, resolution, dt, evolutions);
}