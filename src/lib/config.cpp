#include "easing_methods.hpp"
#include <config.hpp>
#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <string>

Config Config::from_args(int argc, char** argv) {
    Config args_cfg;
    Config file_cfg;

    int opt;
    int long_opt_index;
    static struct option long_opts[] = {
        {"zoom-x-start", required_argument, 0, 0},
        {"zoom-x-end", required_argument, 0, 0},
        {"zoom-y-start", required_argument, 0, 0},
        {"zoom-y-end", required_argument, 0, 0},
        {"x-start", required_argument, 0, 0},
        {"x-end", required_argument, 0, 0},
        {"y-start", required_argument, 0, 0},
        {"y-end", required_argument, 0, 0},
        {0, 0, 0, 0}
    };

    while((opt = getopt_long(argc, argv, "x:y:f:s:i:o:c:", long_opts, &long_opt_index)) != -1) {
        const glm::ivec2& res = args_cfg.resolution.get();
        switch(opt) {
        case 'x':
            args_cfg.resolution.set({(int)strtol(optarg, nullptr, 10), res.y});
            break;
        case 'y':
            args_cfg.resolution.set({res.x, (int)strtol(optarg, nullptr, 10)});
            break;
        case 'f':
            args_cfg.frames.set((int)strtol(optarg, nullptr, 10));
            break;
        case 's':
            args_cfg.steps.set((int)strtol(optarg, nullptr, 10));
        case 'i':
            args_cfg.input.set(optarg);
            break;
        case 'o':
            args_cfg.output.set(optarg);
            break;
        case 'c':
            file_cfg = nlohmann::json::parse(std::ifstream(optarg)).get<Config>();
            break;
        default:
            fprintf(stderr, "Warning: unrecognized option %c\n", opt);
        }
    }

    file_cfg.merge(args_cfg);
    return file_cfg;
}

void Config::merge(const Config& c) {
    if(c.resolution.isSet()) {
        this->resolution = c.resolution;
    }
    if(c.zoom_start.isSet()) {
        this->zoom_start = c.zoom_start;
    }
    if(c.zoom_end.isSet()) {
        this->zoom_end = c.zoom_end;
    }
    if(c.frames.isSet()) {
        this->frames = c.frames;
    }
    if(c.steps.isSet()) {
        this->steps = c.steps;
    }
    if(c.input.isSet()) {
        this->input = c.input;
    }
    if(c.output.isSet()) {
        this->output = c.output;
    }
}

template<typename T>
const T& ConfigField<T>::get() const {
    return this->val;
}

template<typename T>
void ConfigField<T>::set(const T& val) {
    this->val = val;
    this->is_set = true;
}

template<typename T>
bool ConfigField<T>::isSet() const {
    return this->is_set;
}

template<typename T>
void ConfigField<T>::unset() {
    this->val = this->def;
    this->is_set = false;
}

void to_json(nlohmann::json& j, const Config& c) {
    if(c.resolution.isSet()) {
        glm::ivec2 res = c.resolution.get();
        j.emplace("resolution", nlohmann::json{
            {"x", res.x},
            {"y", res.y}
        });
    }

    if(c.zoom_start.isSet()) {
        glm::mat2x2 res = c.zoom_start.get();
        j.emplace("zoom_start", nlohmann::json{
            {"top_left", nlohmann::json{
                {"x", (res)[0][0]},
                {"y", (res)[0][1]}}},
            {"bottom_right", nlohmann::json{
                {"x", (res)[1][0]},
                {"y", (res)[1][1]}}}
        });
    }

    if(c.zoom_end.isSet()) {
        glm::mat2x2 res = c.zoom_end.get();
        j.emplace("zoom_end", nlohmann::json{
            {"top_left", nlohmann::json{
                {"x", (res)[0][0]},
                {"y", (res)[0][1]}}},
            {"bottom_right", nlohmann::json{
                {"x", (res)[1][0]},
                {"y", (res)[1][1]}}}
        });
    }

    if(c.easing_method.isSet()) {
        j.emplace("easing_method", *c.easing_method.get()->getName());
    }

    if(c.steps.isSet()) {
        j.emplace("steps", c.steps.get());
    }

    if(c.input.isSet()) {
        j.emplace("input", c.input.get());
    }

    if(c.output.isSet()) {
        j.emplace("output", c.output.get());
    }
}

void from_json(const nlohmann::json& j, Config& c) {
    try {
        c.resolution.set({j.at("resolution").at("x"), j.at("resolution").at("y")});
    } catch(const nlohmann::json::out_of_range& e) {}

    try {
        c.zoom_start.set({
            {j.at("zoom_start").at("top_left").at("x").get<float>(), j.at("zoom_start").at("top_left").at("y").get<float>()},
            {j.at("zoom_start").at("bottom_right").at("x").get<float>(), j.at("zoom_start").at("bottom_right").at("y").get<float>()}
        });
    } catch(const nlohmann::json::out_of_range& e) {}

    try {
        c.steps.set(j.at("steps").get<int>());
    } catch(const nlohmann::json::out_of_range& e) {}

    try {
        c.zoom_end.set({
            {j.at("zoom_end").at("top_left").at("x").get<float>(), j.at("zoom_end").at("top_left").at("y").get<float>()},
            {j.at("zoom_end").at("bottom_right").at("x").get<float>(), j.at("zoom_end").at("bottom_right").at("y").get<float>()},
        });
    } catch(const nlohmann::json::out_of_range& e) {}

    try {
        c.frames.set(j.at("frames").get<int>());
    } catch(const nlohmann::json::out_of_range& e) {}

    try {
        c.input.set(j.at("input").get<std::string>().c_str());
    } catch(const nlohmann::json::out_of_range& e) {}
    
    try {
        c.output.set(j.at("output").get<std::string>().c_str());
    } catch(const nlohmann::json::out_of_range& e) {}

    try {
        EasingMethod* em = new EasingMethod(EasingMethod::from_json(j.at("easing_method")));
        c.easing_method.set(em);
    } catch(const nlohmann::json::out_of_range& e) {}
}