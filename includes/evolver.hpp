#ifndef FRACGEN_EVOLVER_HPP
#define FRACGEN_EVOLVER_HPP

#define cimg_use_png
#define cimg_display 0

#include <glm/gtx/component_wise.hpp>
#include <CImg.h>

#include <particle.hpp>
#include <config.hpp>

void dispatch_kernel_evolve(particle* particles, glm::ivec2 resolution, float dt, int evolutions);

class Evolver {
private:
    glm::mat3x2 gravitators = {
        {-.5, -.5},
        { .5, -.5},
        {  0,  .5}
    };
    unsigned char gravitator_colors[3][3] = {
        {255,0,0},
        {0,255,0},
        {0,0,255}
    };
    glm::mat2x2 camera;
    glm::ivec2  resolution;
    bool invalidated = false;
    particle* particle_cache;
    particle* device_particles;
public:
    static constexpr glm::mat2x2 default_camera = {
        {-10, -10},
        { 10,  10}
    };

    Evolver(const Config& cfg, const glm::mat2x2 camera = default_camera);

    ~Evolver();

    void evolve(float seconds, int evolutions=1);

    cimg_library::CImg<unsigned char> render();

    const particle& operator[](glm::ivec2 ind);
};

#endif