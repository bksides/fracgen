#ifndef FRACGEN_ANIMATION_HPP
#define FRACGEN_ANIMATION_HPP

#include <easing_methods.hpp>
#include <interpolator.hpp>

class Animation {
private:
    const Interpolator& start;
    const Interpolator& end;
public:
    const EasingMethod em;
    Animation(const Interpolator& start, const Interpolator& end, const EasingMethod& em) : start(start), end(end), em(em) {}
    const Interpolator& state_at(float animation_progress) const{
        Interpolator* interpolation = start.interpolate(&end, em(animation_progress));
        return *interpolation;
    }
};

#endif